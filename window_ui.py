# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(811, 662)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.acquiringButton = QtWidgets.QPushButton(self.centralwidget)
        self.acquiringButton.setGeometry(QtCore.QRect(130, 510, 75, 23))
        self.acquiringButton.setObjectName("acquiringButton")
        self.programStatuslabel = QtWidgets.QLabel(self.centralwidget)
        self.programStatuslabel.setGeometry(QtCore.QRect(130, 540, 181, 20))
        self.programStatuslabel.setText("")
        self.programStatuslabel.setObjectName("programStatuslabel")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(-1, -1, 801, 491))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.graphWidget = PlotWidget(self.verticalLayoutWidget)
        self.graphWidget.setObjectName("graphWidget")
        self.verticalLayout.addWidget(self.graphWidget)
        self.lslButton = QtWidgets.QPushButton(self.centralwidget)
        self.lslButton.setGeometry(QtCore.QRect(330, 510, 75, 23))
        self.lslButton.setObjectName("lslButton")
        self.connectButton = QtWidgets.QPushButton(self.centralwidget)
        self.connectButton.setGeometry(QtCore.QRect(20, 510, 75, 23))
        self.connectButton.setObjectName("connectButton")
        self.lslStatusLabel = QtWidgets.QLabel(self.centralwidget)
        self.lslStatusLabel.setGeometry(QtCore.QRect(330, 540, 181, 20))
        self.lslStatusLabel.setText("")
        self.lslStatusLabel.setObjectName("lslStatusLabel")
        self.fileName = QtWidgets.QTextEdit(self.centralwidget)
        self.fileName.setGeometry(QtCore.QRect(540, 540, 201, 31))
        self.fileName.setObjectName("fileName")
        self.checkBoxSaveFile = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBoxSaveFile.setGeometry(QtCore.QRect(470, 510, 81, 20))
        self.checkBoxSaveFile.setObjectName("checkBoxSaveFile")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(470, 550, 71, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 550, 71, 16))
        self.label_2.setObjectName("label_2")
        self.portName = QtWidgets.QTextEdit(self.centralwidget)
        self.portName.setGeometry(QtCore.QRect(20, 570, 91, 31))
        self.portName.setObjectName("portName")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 811, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.acquiringButton.setText(_translate("MainWindow", "Acquire"))
        self.lslButton.setText(_translate("MainWindow", "LSL"))
        self.connectButton.setText(_translate("MainWindow", "Connect"))
        self.fileName.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.checkBoxSaveFile.setText(_translate("MainWindow", "Save file"))
        self.label.setText(_translate("MainWindow", "File name:"))
        self.label_2.setText(_translate("MainWindow", "Port name:"))
        self.portName.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))

from pyqtgraph import PlotWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
