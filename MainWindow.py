from PyQt5.QtWidgets import QMessageBox
from window_ui import *
from OpenBCIConnection import OpenBCIConnection

from EEGPlot import EEGPlot
from datetime import datetime

import os
import numpy as np

# pyuic5 -x window.ui -o window_ui.py

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    SAMPLE_SIZE = 1000;

    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)

        self.lslStatusLabel.setText("Desactivated")
        self.lslButton.setEnabled(False)
        self.acquiringButton.setEnabled(False)
        self.fileName.setEnabled(False)
        self.checkBoxSaveFile.setEnabled(False)

        self.connectButton.clicked.connect(self.connect)
        self.acquiringButton.clicked.connect(self.acquireData)
        self.lslButton.clicked.connect(self.changeLSLStatus)

        self.connected = False
        self.eegValues = list()
        self.createClearSignalList()
        self.openBCIConnection = None

        self.plotFigure = EEGPlot(self.graphWidget, self.SAMPLE_SIZE, self.eegValues)

    def connect(self):
        port = self.portName.toPlainText()

        if (len(port) == 0):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("Define connection port.")
            msg.setWindowTitle("Port Error")

            msg.exec_()

        else:
            self.openBCIConnection = OpenBCIConnection(port, self.eegValues, self.plotFigure, list(np.zeros(self.SAMPLE_SIZE)))
            self.openBCIConnection.finished.connect(self.acquisitionFinished)
            self.connectButton.setEnabled(False)
            self.lslButton.setEnabled(True)
            self.acquiringButton.setEnabled(True)
            self.fileName.setEnabled(True)
            self.checkBoxSaveFile.setEnabled(True)
            self.portName.setEnabled(False)

    def acquireData(self):
        self.programStatuslabel.setText("Acquiring data...")
        self.lslButton.setEnabled(False)
        self.fileName.setEnabled(False)
        self.checkBoxSaveFile.setEnabled(False)

        self.initializeConnection()

    def changeLSLStatus(self):
        if self.openBCIConnection.getSendLSL() == True:
            self.openBCIConnection.setSendLSL(False)
            self.lslStatusLabel.setText("Desactivated")
        else:
            self.openBCIConnection.setSendLSL(True)
            self.lslStatusLabel.setText("Activated")

    def initializeConnection(self):
        if self.connected == False:
            self.openBCIConnection.start()
            self.connected = True

        else:
            self.openBCIConnection.stop()
            self.connected = False

    def acquisitionFinished(self):
        self.programStatuslabel.setText("Connection finished!")
        self.deleteConnection()

        if self.checkBoxSaveFile.isChecked():
            self.saveFile()

        self.lslButton.setEnabled(True)
        self.fileName.setEnabled(True)
        self.checkBoxSaveFile.setEnabled(True)

        self.createClearSignalList()
        self.openBCIConnection.setMarkers(list(np.zeros(self.SAMPLE_SIZE)));

    def deleteConnection(self):
        self.openBCIConnection.stop()

    def createClearSignalList(self):
        self.eegValues.clear()

        for i in range(16):
            self.eegValues.append(list(np.zeros(self.SAMPLE_SIZE)))

    def saveFile(self):
        now = datetime.now()

        folder = 'records/'
        format = '.csv'
        fileName = self.fileName.toPlainText() + format

        if (len(fileName) == len(format)):
            fileName = now.strftime('%Y%d%m_%H%M%S' + format)

        fileName = folder + fileName

        if not os.path.isdir(folder):
            try:
                os.mkdir(folder)
            except OSError:
                print ("Creation of the directory %s failed" % folder)

        with open(fileName, 'w') as file:

            for i in range(len(self.eegValues)):
                values = ', '.join(map(str, self.eegValues[i]))
                file.write(values)
                file.write('\n')

            file.write(', '.join(map(str, self.openBCIConnection.getMarkers())))
